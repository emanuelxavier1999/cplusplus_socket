#ifndef expection_hpp
#define expection_hpp

#include <string>

namespace Exception {
    class Exception {
        private:
            std::string _message;
        public:
            Exception(std::string error) { this->_message = error; }
            virtual const char* what() { return this->_message.c_str(); }
    };
}

#endif