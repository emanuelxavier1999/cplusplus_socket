#ifndef server_hpp
#define server_hpp

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "./expection.hpp"

namespace Server_socket {
    class TCP_IPV4 {
    private:
        int port;
        int buffer_len;
        char *buffer;
        bool binded;
        struct sockaddr_in client, server;
        int clientfd, serverfd;
        int yes;

    public:
        TCP_IPV4(int port, int buffer_len);
        ~TCP_IPV4(); 
        int _recv(char* message);
        int _send(char* message);
        void _close();
        int _bind();
        int _accept();
        int _listen();
    };

    TCP_IPV4::TCP_IPV4(int port, int buffer_len){
        this->port = port;
        this->buffer_len = buffer_len;
        buffer = new char[buffer_len];

        serverfd = socket(AF_INET, SOCK_STREAM, 0);
        if(serverfd == -1)
            throw Exception::Exception("[Constructor] Can't create the server socket:");

        server.sin_family = AF_INET;
        server.sin_port = htons(port);
        server.sin_addr.s_addr = htonl(INADDR_ANY);
        memset(server.sin_zero, 0x0, 8);

        yes = 1;
        if(setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) // Handle the error of the port already in use 
            throw Exception::Exception("Socket options error:");

        binded = false;
    }

    TCP_IPV4::TCP_IPV4::~TCP_IPV4(){
        delete buffer;
    }

    int TCP_IPV4::_recv(char* message){
        memset(buffer, 0x0, buffer_len);
        int len = recv(clientfd, buffer, buffer_len, 0);
        buffer[len + 1] = '\0';
        strcpy(message, buffer);
        return len;
    }

    int TCP_IPV4::_send(char* message){
        return send(clientfd, message, strlen(message), 0);
    }

    void TCP_IPV4::_close(){
        close(clientfd);
        close(serverfd);
        binded = false;
    }

    int TCP_IPV4::_bind(){

        if(binded)
            TCP_IPV4::_close();

        int b = bind(serverfd, (struct sockaddr*)&server, sizeof(server));

        if(b == -1) 
            throw Exception::Exception("Socket bind error");

        binded = true;

        return b;
    }

    int TCP_IPV4::_accept(){
        socklen_t client_len = sizeof(client);
        int a = (clientfd=accept(serverfd, (struct sockaddr *) &client, &client_len));

        if(a == -1) 
            throw Exception::Exception("Accept error");

        return a;
    }

    int TCP_IPV4::_listen(){
        int l = listen(serverfd, 1);

        if(l == -1) 
            throw Exception::Exception("Listen error");

        return l;
    }

}

#endif