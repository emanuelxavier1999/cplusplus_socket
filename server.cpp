#include "./server.hpp"

#include <iostream>

using namespace std;

int main () {

    try {

        Server_socket::TCP_IPV4 server(3333, 300);
        cout << "binding socket" << endl;
        server._bind();
        cout << "listening socket" << endl;
        server._listen();
        cout << "accpeting socket" << endl;
        server._accept();

        char buffer[4242];
        strcpy(buffer, "Hello, client");
        cout << "sending message" << endl;

        int message_len;
        if(server._send(buffer)) {
            cout << "message sended" << endl;
            while (1) {
                memset(buffer, 0x0, 300);
                if((message_len = server._recv(buffer)) > 0) {
                    buffer[message_len + 1] = '\0';
                    printf("Client says: %s\n", buffer);
                }
                if(strcmp(buffer, "exit") == 0) {
                    strcpy(buffer, "bye");
                    server._send(buffer);
                    break;
                } else {
                    strcpy(buffer, "message recived");
                    server._send(buffer);
                }
            }
        }

        cout << "closing server" << endl;
        server._close();
    }
    catch(Exception::Exception &e) {
        cout << e.what() << endl;
    }

    return 0;
}