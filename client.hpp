#ifndef socket_hpp
#define socket_hpp

#include <string>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "expection.hpp"

namespace Client_socket {

    class TCP_IPV4 {
    private:
        int port;
        std::string address;
        int buffer_len;
        char *buffer;
        bool binded;
        struct sockaddr_in server;
        int socketfd;

    public:
        TCP_IPV4(int port, std::string address, int buffer_len);
        ~TCP_IPV4();
        virtual int _connect ();
        virtual int _recv(char* message);
        virtual void _send(char* message);
        virtual void _close();
        virtual int _bind();
    };

    TCP_IPV4::TCP_IPV4(int port, std::string address, int buffer_len) {
        if ((socketfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
            throw Exception::Exception("[Constructor] Cannot create socket");

        this->port = port; 
        this->address = address;
        this->buffer_len = buffer_len;
        buffer = new char[buffer_len];
        binded = false;

        server.sin_family = AF_INET;
        server.sin_port = htons(port);
        server.sin_addr.s_addr = inet_addr(address.c_str());
        memset(server.sin_zero, 0x0, 8);
    }

    TCP_IPV4::TCP_IPV4::~TCP_IPV4() {
        delete buffer;
    }

    int TCP_IPV4::_connect() {
        int connection = connect(socketfd, (struct sockaddr*) &server, sizeof(server));
        if (connection == -1)
            throw Exception::Exception("[Connect] Cannot connect socket");

        return connection;
    }

    int TCP_IPV4::_recv(char* message) {
        memset(buffer, 0x0, buffer_len);
        int len = recv(socketfd, buffer, buffer_len, 0);
        buffer[len + 1] = '\0';
        strcpy(message, buffer);
        return len;
    }

    void TCP_IPV4::_send(char* message) {
        send(socketfd, message, strlen(message), 0);
    }

    void TCP_IPV4::_close() { 
        close(socketfd);
    }

    int TCP_IPV4::_bind() { return 0; }
}

#endif